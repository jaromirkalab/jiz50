#!/bin/sh
# input rules for REAL_PORT and SIMU_PORT must be enabled in iptables!


TABLE_CMD="iptables -t nat -L -n --line-numbers"
DST_PORT=80
REAL_PORT=8585
SIMU_PORT=8686

switch_ports() {
        OLD_PORT=$1
        NEW_PORT=$2

        iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport $DST_PORT -j REDIRECT --to-ports $OLD_PORT
        iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport $DST_PORT -j REDIRECT --to-ports $NEW_PORT

        timeout -k 5 30 tcpkill -i eth0 port $DST_PORT
}


if  $TABLE_CMD | grep "dpt:$DST_PORT redir ports $REAL_PORT" ; then
        echo "Running from real data (port $REAL_PORT)";
        switch_ports $REAL_PORT $SIMU_PORT

elif $TABLE_CMD | grep "tcp dpt:$DST_PORT redir ports $SIMU_PORT" ; then
        echo "Running from simulated race (port $SIMU_PORT)";

        switch_ports $SIMU_PORT $REAL_PORT
else
        echo "Not running?";
fi

$TABLE_CMD;

exit;
