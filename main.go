package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/pressly/chi"
	"github.com/pressly/chi/middleware"

	"bitbucket.org/oisw/jiz50/event"
	"bitbucket.org/oisw/jiz50/mock"
)

func tvSimplePage(tmplDir string, ev *event.Event) func(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseGlob(filepath.Join(tmplDir, "*.tmpl"))
	if err != nil {
		panic(fmt.Errorf("tvSimplePage templates: %s", err.Error()))
	}

	return func(w http.ResponseWriter, r *http.Request) {
		i := ev.CreateStatusInfo()
		t.Execute(w, i)
	}
}

func tvSimplePageText(ev *event.Event) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		i := ev.CreateStatusInfo()
		w.Header().Set("Content-type", "text/plain")
		w.Write([]byte(fmt.Sprintf("%d", i.Distance)))
	}
}

func baseRouter(assetsDir string) chi.Router {

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.CloseNotify)
	r.Use(middleware.Timeout(20 * time.Second))

	for _, p := range []string{"css", "img", "js", "fonts"} {
		r.FileServer("/"+p, http.Dir(filepath.Join(assetsDir, p)))
	}
	return r
}

func main() {
	var (
		feedUrl, host, port, assetsDir, templatesDir, htmlDir string
		fakeFeed                                              *mock.Race
	)
	host = "localhost"
	port = "8585"

	if hostport := os.Getenv("HOSTPORT"); hostport != "" {
		t := strings.Split(hostport, ":")
		host = t[0]
		if len(t) == 2 {
			port = t[1]
		}
	}

	switch {
	case os.Getenv("SELF_RACE") != "":
		feedUrl = fmt.Sprintf("http://%s:%s/source", host, port)
	case os.Getenv("FEED_URL") != "":
		feedUrl = os.Getenv("FEED_URL")
	default:
		feedUrl = "http://jiz502020.sportsoft.cz/getsplits.aspx"
	}

	if htmlDir = os.Getenv("HTML_DIR"); htmlDir == "" {
		wd, _ := os.Getwd()
		htmlDir = filepath.Join(wd, "html")
	}

	if assetsDir = os.Getenv("ASSETS_DIR"); assetsDir == "" {
		wd, _ := os.Getwd()
		assetsDir = filepath.Join(wd, "assets")
	}

	if templatesDir = os.Getenv("TEMPLATES_DIR"); templatesDir == "" {
		wd, _ := os.Getwd()
		templatesDir = filepath.Join(wd, "templates")
	}

	r := baseRouter(assetsDir)

	if os.Getenv("SELF_RACE") != "" {
		fakeFeed = mock.NewRace(5000, []int{0, 8000, 16000, 24000, 32000, 50000})
		r.Get("/source", fakeFeed.ServeHTTP)
	}

	event := event.NewEvent(feedUrl)
	event.Start()

	r.HandleFunc("/ws", event.WebSocketHandler)
	r.HandleFunc("/adminws", event.WebSocketAdminHandler)

	// page with running skiers
	r.Get("/tvfeed", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filepath.Join(htmlDir, "tvfeed", "index.html"))
	})
	// simplified page for press
	r.Get("/news", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filepath.Join(htmlDir, "news", "index.html"))
	})

	// page wit number of kilometers only - must be reloaded
	r.Get("/tv", tvSimplePageText(event))

	r.Mount("/admin/", adminRouter(filepath.Join(templatesDir, "admin"), fakeFeed, event))

	log.Printf("Starting listen on port %s:%s, feed URL: '%s'", host, port, feedUrl)
	http.ListenAndServe(host+":"+port, r)
}
