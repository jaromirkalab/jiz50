package event

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// Test limitation of the XML reader (buffer overflow protection)
func TestXMLOverSizeLimit(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, strings.Repeat("a", maxXMLSize))
	}))
	defer ts.Close()

	ev := NewEvent(ts.URL)
	_, err := ev.refresh()

	if err == nil || !strings.Contains(err.Error(), "body size is over limit") {
		t.Fatalf("Expected XML size overflow error, got '%v'", err)
	}
}

// Test  XML reader
func TestXMLInvalidUnderLimit(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, strings.Repeat("a", maxXMLSize-2))
	}))
	defer ts.Close()

	ev := NewEvent(ts.URL)
	_, err := ev.refresh()

	if err == nil || !strings.Contains(err.Error(), "can't unmarshal XML") {
		t.Fatalf("Expected XML unmarshall error, got '%v'", err)
	}
}
