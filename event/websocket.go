package event

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)


// WebSocketHandler feeds standard pages after every
// update of data through channel
func (e *Event) WebSocketHandler(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	ch := make(chanStatusInfo, 32)
	// enqueue current status immediatelly to inform page about status
	ch <- e.CreateStatusInfo()

	wc := wsClient{
		IP:        r.RemoteAddr,
		UserAgent: r.Header.Get("User-Agent"),
		InfoChan:  ch,
		CreatedAt: time.Now(),
	}

	// register a new websocket client
	e.Lock()
	e.wsClients[conn] = &wc
	e.Unlock()

	go func() {
		for {
			if _, _, err := conn.NextReader(); err != nil {
				e.Lock()
				delete(e.wsClients, conn)
				e.Unlock()
				close(wc.InfoChan)
				log.Printf("Closing websocket IP: %s, agent %s", wc.IP, wc.UserAgent)
				break
			}
		}
	}()

	defer conn.Close()

	// listen for a new mesage and send it to websocket
	for {
		if info, ok := <-ch; ok {
			conn.WriteJSON(info)
			wc.Messages++ // for statistics only
		} else {
			log.Printf("Send channel for websocket closed. IP: %s, agent %s", wc.IP, wc.UserAgent)
			break
		}
	}
}

// WebSocketAdminHandler feeds admin page with internal
// information every second regardles feed url is working or not
func (e *Event) WebSocketAdminHandler(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	defer conn.Close()
	finish := false

	go func() {
		for {
			if _, _, err := conn.NextReader(); err != nil {
				finish = true
				break
			}
		}
	}()

	for !finish {
		i := e.CreateStatusInfo()

		e.RLock()
		realDist := e.distance()

		resp := struct {
			Paused         bool       `json:"paused"`
			MessagesTotal  int        `json:"messagesTotal"`
			Errors         int        `json:"errors"`
			RealDistance   int        `json:"realDistance"`
			FeedURL        string     `json:"feedURL"`
			LastError      string     `json:"lastError"`
			LastErrorAt    *time.Time `json:"lastErrorAt"`
			FirstMessageAt *time.Time `json:"firstMessageAt"`
			LastMessageAt  *time.Time `json:"lastMessageAt"`
			StatusInfo
		}{
			e.paused,
			e.messagesReceived,
			e.errorCount,
			realDist,
			e.feedURL,
			"",
			nil,
			nil,
			nil,
			i,
		}
		if !e.firstDataAt.IsZero() {
			resp.FirstMessageAt = &e.firstDataAt
		}
		if e.lastError != nil {
			resp.LastError = e.lastError.Error()
			resp.LastErrorAt = &e.lastErrorAt
		}
		if e.lastSnapshot != nil {
			t := e.lastSnapshot.Received()
			resp.LastMessageAt = &t
		}
		e.RUnlock()

		conn.WriteJSON(resp)
		time.Sleep(1 * time.Second)
	}
}
