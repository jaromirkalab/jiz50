package event

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"bitbucket.org/oisw/jiz50/feed"
	"github.com/gorilla/websocket"
)

// To protect reader from overflow attack - only 2k bytes
// are read from http request
const maxXMLSize = 2048

// StatusInfo will be sent through websocket to every
// client page
type StatusInfo struct {
	Distance       int `json:"distance"`
	SkiersOnTrack  int `json:"skiersOnTrack"`
	SkiersAtFinish int `json:"skiersAtFinish"`
}

type chanStatusInfo chan StatusInfo

// wsClient is a structure for one registeres websocket client
type wsClient struct {
	IP        string
	UserAgent string
	Messages  int
	CreatedAt time.Time
	InfoChan  chanStatusInfo
}

// Event is container that gathers data from XML feed about
// checkpoints
type Event struct {
	firstDataAt          time.Time      // first non-zero snapshot received
	lastSnapshot         *feed.Snapshot // stored last valid snapshot
	lastReceivedSnapshot *feed.Snapshot // stored last received snapshot
	lastError            error
	lastErrorAt          time.Time
	errorCount           int
	messagesReceived     int
	wsClients            map[*websocket.Conn]*wsClient
	netClient            *http.Client // client for feed
	finish               chan interface{}
	feedURL              string
	paused               bool // paused feed receiving
	// approximation is used at very beginning of race
	approxDistance float64
	approxAt       time.Time
	exportChannel  chan *feed.Snapshot
	sync.RWMutex
}

// NewEvent creates a new Event structure - main race container
func NewEvent(url string) *Event {
	return &Event{
		feedURL:   url,
		wsClients: make(map[*websocket.Conn]*wsClient),
		netClient: &http.Client{
			Timeout: time.Second * 15,
			Transport: &http.Transport{
				Dial: (&net.Dialer{
					Timeout:   10 * time.Second,
					KeepAlive: 0,
				}).Dial,
				//KeepAlive disable after Bedrichov30 because
				// of problem with server sportsoft.cz
				DisableKeepAlives:   true,
				TLSHandshakeTimeout: 5 * time.Second,
			},
		},
	}
}

// Start installs go routine to fetch data from source feed
func (e *Event) Start() {

	if e.finish != nil {
		// probably running
		return
	}

	// channel for stopping
	e.finish = make(chan interface{})

	go func() {
		for {
			alarm := time.After(time.Duration(5+rand.Intn(6)) * time.Second)

			select {
			case <-e.finish:
				return
			case <-alarm:
				_, err := e.refresh()
				if err != nil {
					e.Lock()
					e.errorCount++
					e.lastError = err
					e.lastErrorAt = time.Now()
					e.Unlock()
					log.Printf("Channel feed error: %s", err.Error())
				}
				e.broadcastInfo()
			}
		}
	}()
}

// fetch one sample XML, parse and decide to store it or not
// as a valid snapshot
func (e *Event) refresh() (bool, error) {

	s, err := e.retrieveSnapshot()

	if err != nil {
		return false, err
	}

	// ok, new data arrives
	e.Lock()
	defer e.Unlock()

	e.messagesReceived++
	e.lastReceivedSnapshot = s

	if e.paused {
		// do not process feed
		return false, nil
	}

	if e.lastSnapshot != nil {
		lDist := e.lastSnapshot.TotalDistance()
		sDist := s.TotalDistance()

		if sDist > 0 && e.firstDataAt.IsZero() {
			// first valid packet with data => race started
			e.firstDataAt = time.Now()
			e.approxAt = time.Now()
			e.activateExporter(fmt.Sprintf("%s.csv", e.firstDataAt.Format(time.RFC3339)))
		}

		if lDist != sDist && e.exportChannel != nil {
			e.exportChannel <- s
		}

		// approximation for very beginning of the race
		speed := 2.5 + rand.Float64()
		e.approxDistance += float64(e.lastSnapshot.SkiersOnTrack()) * speed * float64(time.Now().Unix()-e.approxAt.Unix())
		e.approxAt = time.Now()

		if lDist > sDist {
			return false, fmt.Errorf("received distance is lower than previous one (%d vs %d)", sDist, lDist)
		}
	}

	// everything seems OK
	e.lastSnapshot = s
	return true, nil
}

// PauseFeed pases loading feed data from source URL
func (e *Event) PauseFeed() {
	e.Lock()
	defer e.Unlock()
	e.paused = true
}

// ResumeFeed resumes paused feed
func (e *Event) ResumeFeed() {
	e.Lock()
	defer e.Unlock()
	e.paused = false
}

// IsPaused checkt whether datafeed is currently paused or not
func (e *Event) IsPaused() bool {
	e.RLock()
	defer e.RUnlock()
	return e.paused
}

func (e *Event) broadcastInfo() {

	info := e.CreateStatusInfo()

	// broadcast info to channels for websockets
	// if channel is full (client is not receiving), skip it
	e.RLock()
	defer e.RUnlock()

	for _, ci := range e.wsClients {
		select {
		case ci.InfoChan <- info:
		default:
			// rare, but...
			log.Printf(
				"detected websocket client %s (%s) with full channel",
				ci.IP, ci.UserAgent)
		}
	}
}

func (e *Event) distance() int {
	if e.lastSnapshot != nil {
		return e.lastSnapshot.TotalDistance() / 1000
	}
	return 0
}

// CreateStatusInfo prepares simple info structure that will be sent
// to clients
func (e *Event) CreateStatusInfo() StatusInfo {
	i := StatusInfo{}

	e.RLock()
	defer e.RUnlock()

	if e.lastSnapshot == nil {
		// no record has been received yet
		return i
	}

	i.SkiersOnTrack = e.lastSnapshot.SkiersOnTrack()
	i.SkiersAtFinish = e.lastSnapshot.SkiersAtFinish()
	i.Distance = e.distance()

	if !e.firstDataAt.IsZero() {
		ad := int(e.approxDistance / 1000)
		if i.Distance < ad {
			// at very beginning of the race data will be approximated
			i.Distance = ad
		}
	}
	return i
}

// ReportWebSockets returns array of connected Websocket clients
func (e *Event) ReportWebSockets() interface{} {
	e.RLock()
	defer e.RUnlock()

	clients := len(e.wsClients)

	r := make([]struct {
		IP, UserAgent, CreatedAt string
		Messages                 int
	}, clients)

	i := 0
	for _, c := range e.wsClients {
		r[i].IP = c.IP
		r[i].UserAgent = c.UserAgent
		r[i].CreatedAt = c.CreatedAt.Format(time.RFC3339)
		r[i].Messages = c.Messages
		i++
	}
	return r
}

// LastReceivedSnapshot returns a complete Snapshot that was received
func (e *Event) LastReceivedSnapshot() *feed.Snapshot {
	e.RLock()
	defer e.RUnlock()
	return e.lastReceivedSnapshot
}

func (e *Event) activateExporter(fname string) {

	if e.exportChannel != nil {
		close(e.exportChannel)
		e.exportChannel = nil
	}

	err := ioutil.WriteFile(fname, []byte{}, 0644)
	if err != nil {
		log.Printf("activate exporter: %s", err.Error())
		return
	}

	e.exportChannel = make(chan *feed.Snapshot, 32)

	go func() {
		var b bytes.Buffer
		t := time.NewTicker(60 * time.Second)

		head := false
		for {
			select {
			case <-e.finish:
				flush(fname, &b)
				t.Stop()
				return
			case s, ok := <-e.exportChannel:
				if !ok {
					// closing channel means flush and finish
					flush(fname, &b)
					break
				}
				if !head {
					fmt.Fprintf(&b, "Date;")
					serialize(&b, s.Distances())
					head = true
				}
				fmt.Fprintf(&b, "%s", s.Received().Format("2.1.2006 15:04:05;"))
				serialize(&b, s.Counts())
			case <-t.C:
				log.Printf("Tick, len: %d", b.Len())
				if b.Len() > 0 {
					flush(fname, &b)
				}
			}
		}
	}()
}

func serialize(b *bytes.Buffer, data []int) {
	s := ""
	for i := 0; i < len(data); i++ {
		fmt.Fprintf(b, "%s%d", s, data[i])
		if s == "" {
			s = ";"
		}
	}
	fmt.Fprintf(b, "\n")
}

func flush(fname string, b *bytes.Buffer) {
	f, err := os.OpenFile(fname, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Printf("snapshot exporter: %s", err.Error())
		return
	}
	defer f.Close()

	_, err = b.WriteTo(f)
	if err != nil {
		log.Printf("snapshot exporter: %s", err.Error())
		return
	}
	b.Reset()
}

func (e *Event) retrieveSnapshot() (*feed.Snapshot, error) {
	r, err := e.netClient.Get(e.feedURL)
	if err != nil {
		return nil, err
	}

	if r.Body != nil {
		defer r.Body.Close()
	}

	if r.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GET '%s' returns code %d", e.feedURL, r.StatusCode)
	}

	// need to protect against buffer overflow
	var buff bytes.Buffer
	size, err := buff.ReadFrom(io.LimitReader(r.Body, maxXMLSize))

	if err != nil {
		return nil, fmt.Errorf("can't read body from response %s", err.Error())
	}

	if size == 0 {
		return nil, fmt.Errorf("response body size is zero")
	}

	if size == maxXMLSize {
		return nil, fmt.Errorf("response body size is over limit")
	}

	s, err := feed.UnmarshalSnapshot(buff.Bytes())

	if err != nil {
		return nil, fmt.Errorf("can't unmarshal XML %s:\n%s", err.Error(), string(buff.Bytes()))
	}

	return s, nil
}
