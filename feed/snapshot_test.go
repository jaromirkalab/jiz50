package feed

import (
	"testing"
)

func TestUnmarshal(t *testing.T) {
	xml := `<splits>
<split distance="11200" count="4030" name="11 km"/>
<split distance="22800" count="4017" name="26 km"/>
<split distance="37100" count="4018" name="37 km"/>
<split distance="45000" count="4026" name="Finish"/>
</splits>
`

	s, err := UnmarshalSnapshot([]byte(xml))

	if err != nil {
		t.Fatalf("unmarshalling produces error: %s", err.Error())
	}

	if num := len(s.checkpoints); num != 4 {
		t.Fatalf("expected 4 splits, got %d", num)
	}

	if ed := 181214800; s.TotalDistance() != ed {
		t.Errorf("snapshot distance error. Got %d, expected %d", s.TotalDistance(), ed)
	}
}

func TestCalculateResult(t *testing.T) {
	tests := []struct {
		Cps              CheckpointList // checkpoints from one snapshot
		ExpDistance      int            // expected metres
		ExpSkiersOnTrack int
	}{
		{
			Cps: []Checkpoint{
				Checkpoint{Distance: 11200, Count: 4030},
				Checkpoint{Distance: 37100, Count: 4018},
				Checkpoint{Distance: 22800, Count: 4017},
				Checkpoint{Distance: 45000, Count: 4028, Name: "Test1"},
			},
			ExpDistance:      45000*4028 + (4030-4028)*11200,
			ExpSkiersOnTrack: 2,
		},
		{
			Cps: []Checkpoint{
				Checkpoint{Distance: 22800, Count: 11},
				Checkpoint{Distance: 11200, Count: 20},
				Checkpoint{Distance: 37100, Count: 3},
				Checkpoint{Distance: 45000, Count: 0, Name: "Test2"},
			},
			ExpDistance:      3*37100 + (11-3)*22800 + (20-11)*11200,
			ExpSkiersOnTrack: 20,
		},
		{
			Cps: []Checkpoint{
				Checkpoint{Distance: 11200, Count: 0},
				Checkpoint{Distance: 22800, Count: 20},
				Checkpoint{Distance: 37100, Count: 3},
				Checkpoint{Distance: 45000, Count: 5, Name: "Test3"},
			},
			ExpDistance:      5*45000 + (20-5)*22800,
			ExpSkiersOnTrack: 15,
		},
		{
			Cps: []Checkpoint{
				Checkpoint{Distance: 11200, Count: 2},
				Checkpoint{Distance: 22800, Count: 0},
				Checkpoint{Distance: 37100, Count: 0},
				Checkpoint{Distance: 45000, Count: 0, Name: "Test4"},
			},
			ExpDistance:      2 * 11200,
			ExpSkiersOnTrack: 2,
		},
		{
			Cps: []Checkpoint{
				Checkpoint{Distance: 11200, Count: 2},
				Checkpoint{Distance: 45000, Count: 0},
				Checkpoint{Distance: 22800, Count: 3},
				Checkpoint{Distance: 37100, Count: 2, Name: "Test5"},
			},
			ExpDistance:      2*37100 + 1*22800,
			ExpSkiersOnTrack: 3,
		},

		// special condition - some skiers went over finish sensor
		{
			Cps: []Checkpoint{
				Checkpoint{Distance: 0, Count: 1453},
				Checkpoint{Distance: 11200, Count: 1200},
				Checkpoint{Distance: 45000, Count: 2, Name: "Special case"},
				Checkpoint{Distance: 22800, Count: 0},
				Checkpoint{Distance: 37100, Count: 0},
			},
			ExpDistance:      1200*11200 + 253*10, // every started skier is counted with 10 meters
			ExpSkiersOnTrack: 1453,
		},
	}

	for _, tc := range tests {
		//t.Logf("Input: %+v\n", tc.Cps)
		s := &Snapshot{
			checkpoints: tc.Cps,
		}

		s.calculateDistanceAndSkiers()

		if s.TotalDistance() != tc.ExpDistance {
			t.Errorf("Expected distance %d, got %d for %+v",
				tc.ExpDistance, s.TotalDistance(), tc.Cps)
		}

		if s.SkiersOnTrack() != tc.ExpSkiersOnTrack {
			t.Errorf("Expected skiers on track %d, got %d for %+v",
				tc.ExpSkiersOnTrack, s.SkiersOnTrack(), tc.Cps)
		}
	}
}
