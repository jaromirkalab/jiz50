package feed

import (
	"encoding/xml"
	"sort"
	"time"
)

// Checkpoint keeps data from XML file about sensor distance, skiers that
// crossed the sensor and name of the point
type Checkpoint struct {
	Distance int    `xml:"distance,attr"` // checkpoint distance in meters
	Count    int    `xml:"count,attr"`    // skiers crossed the checkpoint
	Name     string `xml:"name,attr"`     // symbolic name
}

// CheckpointList is an array of all Checkpoints read from XML
type CheckpointList []Checkpoint

// only for XML unmarshalling
type snapshotXML struct {
	Checkpoints CheckpointList `xml:"split"`
}

// Snapshot is one sample from feed source
type Snapshot struct {
	checkpoints    CheckpointList
	receivedAt     time.Time
	totalDistance  int
	skiersOnTrack  int
	skiersAtFinish int
}

// UnmarshalSnapshot decodes one sample from simple XML
//
// ex. <splits>
//        	<split distance="0" count="338" name="Start"/>
//			<split distance="20000" count="407" name="Hřebínek"/>
//			<split distance="30000" count="403" name="Cíl"/>
//	  </splits>
func UnmarshalSnapshot(xmlstring []byte) (*Snapshot, error) {
	s := &snapshotXML{}

	err := xml.Unmarshal(xmlstring, s)

	if err != nil {
		return nil, err
	}

	res := &Snapshot{
		checkpoints: s.Checkpoints,
		receivedAt:  time.Now(),
	}
	res.calculateDistanceAndSkiers()
	return res, nil
}

// calculateDistanceAndSkiers sorts checkpoints descending
// and calculates key values:
//     * number of skiers on track
//     * number of skiers at finish
//     * total of distance (skiers multiplied by their distances)
func (s *Snapshot) calculateDistanceAndSkiers() {
	s.totalDistance, s.skiersOnTrack, s.skiersAtFinish = 0, 0, 0

	// empty record?
	if len(s.checkpoints) == 0 {
		return
	}

	sort.Sort(sort.Reverse(s.checkpoints))

	// no contestant is expected to be in fininish if the previous checkpoint
	// counts nobody. This hack prevents the situation when start and finish line
	// are in the same place and some skiers come over finish sensor
	// happened in 2017 at least twice...
	if s.checkpoints[1].Count == 0 {
		s.checkpoints[0].Count = 0
	}

	var lastCount, maxCount int

	for _, c := range s.checkpoints {
		if c.Count > maxCount {
			// sensors at start line ofter misses skiers, so we can't
			// count on start line sensor for total skiers started
			maxCount = c.Count
		}

		if lastCount < c.Count {
			dist := c.Distance
			if dist == 0 {
				// hack - if checkpoint is exactly at start line
				// minimal distance is calculated to count first meters
				dist = 10
			}
			diff := c.Count - lastCount
			s.totalDistance += diff * dist
			lastCount = c.Count
		}
	}

	s.skiersAtFinish = s.checkpoints[0].Count

	skiers := maxCount - s.skiersAtFinish
	if skiers < 0 {
		// yes, it can happen due to sensor error
		skiers = 0
	}
	s.skiersOnTrack = skiers
}

// CheckpointList returns an array of Checkopints from current snapshot
func (s *Snapshot) CheckpointList() CheckpointList {
	return s.checkpoints
}

// Distances returns an array of distance from finish to start
func (s *Snapshot) Distances() []int {
	l := make([]int, len(s.checkpoints))
	for i := 0; i < len(s.checkpoints); i++ {
		l[i] = s.checkpoints[i].Distance
	}
	return l
}

// Counts returns an array of skiers from finish to start
func (s *Snapshot) Counts() []int {
	l := make([]int, len(s.checkpoints))
	for i := 0; i < len(s.checkpoints); i++ {
		l[i] = s.checkpoints[i].Count
	}
	return l
}

// TotalDistance returns calculated total meters of all skiers
func (s *Snapshot) TotalDistance() int {
	return s.totalDistance
}

// SkiersOnTrack returns current number of skiers that should be on track
func (s *Snapshot) SkiersOnTrack() int {
	return s.skiersOnTrack
}

// SkiersAtFinish returns number of skiers that reached finish line
func (s *Snapshot) SkiersAtFinish() int {
	return s.skiersAtFinish
}

// Received returns timestamp of the record
func (s *Snapshot) Received() time.Time {
	return s.receivedAt
}

func (cl CheckpointList) Len() int {
	return len(cl)
}

func (cl CheckpointList) Swap(i, j int) {
	cl[i], cl[j] = cl[j], cl[i]
}

func (cl CheckpointList) Less(i, j int) bool {
	return cl[i].Distance < cl[j].Distance
}
