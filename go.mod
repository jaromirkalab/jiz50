module bitbucket.org/oisw/jiz50

go 1.13

require (
	github.com/gorilla/websocket v1.1.1-0.20170124233753-4e4c8d08b4b9
	github.com/pressly/chi v2.0.1-0.20170119023758-bb4b58688bb9+incompatible
)
