package main

import (
	"fmt"
	"html/template"
	"net/http"
	"path/filepath"

	"github.com/pressly/chi"

	"bitbucket.org/oisw/jiz50/event"
	"bitbucket.org/oisw/jiz50/feed"
	"bitbucket.org/oisw/jiz50/mdw"
	"bitbucket.org/oisw/jiz50/mock"
)

func adminAuthMDW() *mdw.BasicAuth {
	// traditional :-)
	auth := mdw.NewBasicAuthenticator("admin", func(u, p string) bool {
		if u == "jarmila" && p == "kamila" {
			return true
		}
		return false
	})

	return auth
}

// adminRouter creates router for /admin
// route is authenticated using basic authentication and
// includes simple status page, start button for simulated race and
// status page of websocket clients
func adminRouter(tmplDir string, mockRace *mock.Race, ev *event.Event) chi.Router {
	t, err := template.ParseGlob(filepath.Join(tmplDir, "*.tmpl"))
	if err != nil {
		panic(fmt.Errorf("admin templates: %s", err.Error()))
	}

	r := chi.NewRouter()
	r.Use(adminAuthMDW().Handler)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		startButton := false
		if mockRace != nil {
			startButton = !mockRace.AlreadyStarted()
		}

		t.ExecuteTemplate(w, "index.tmpl", struct {
			StartButton bool
		}{StartButton: startButton})
	})

	r.Post("/start", func(w http.ResponseWriter, r *http.Request) {
		if mockRace != nil && !mockRace.AlreadyStarted() {
			mockRace.Start()
		}
		w.Header().Set("Location", "/admin/")
		w.WriteHeader(http.StatusSeeOther)
	})

	r.Get("/clients", func(w http.ResponseWriter, r *http.Request) {
		res := ev.ReportWebSockets()
		t.ExecuteTemplate(w, "clients.tmpl", res)
	})

	r.Get("/snapshot", func(w http.ResponseWriter, r *http.Request) {
		s := ev.LastReceivedSnapshot()

		var res struct {
			ReceivedAt  string
			Paused      bool
			Checkpoints []feed.Checkpoint
		}

		res.Paused = ev.IsPaused()

		if s == nil {
			// no snapshot stored
			res.ReceivedAt = "hasn't been received yet"
			res.Checkpoints = []feed.Checkpoint{}
		} else {

			cp := s.CheckpointList()
			res.ReceivedAt = s.Received().Format("2.1. 15:04:05")
			res.Checkpoints = make([]feed.Checkpoint, len(cp))
			j := len(cp) - 1
			for i := 0; i < len(cp); i++ {
				res.Checkpoints[i].Count = cp[j].Count
				res.Checkpoints[i].Distance = cp[j].Distance
				res.Checkpoints[i].Name = cp[j].Name
				j--
			}
		}

		t.ExecuteTemplate(w, "snapshot.tmpl", res)
	})

	r.Get("/feedcontrol", func(w http.ResponseWriter, r *http.Request) {
		t.ExecuteTemplate(w, "feedcontrol.tmpl", struct{ Paused bool }{Paused: ev.IsPaused()})
	})

	r.Post("/pause", func(w http.ResponseWriter, r *http.Request) {
		ev.PauseFeed()
		w.Header().Set("Location", "/admin/")
		w.WriteHeader(http.StatusSeeOther)
	})
	r.Post("/resume", func(w http.ResponseWriter, r *http.Request) {
		ev.ResumeFeed()
		w.Header().Set("Location", "/admin/")
		w.WriteHeader(http.StatusSeeOther)
	})

	return r
}
