package mock

// A complete mock to test application including simulating
// Jizerska 50 race

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type checkpoint struct {
	Distance int    `xml:"distance,attr"`
	Count    int    `xml:"count,attr"`
	Name     string `xml:"name,attr"`
}

type checkpointList []checkpoint

// only for XML marshalling
type snapshotXML struct {
	Checkpoints checkpointList `xml:"split"`
}

type contestant struct {
	Distance float64
	Speed    float64
}

type contestantList []contestant

// Race is fictional race producing XML feed with
// simulated constants
type Race struct {
	onTrack     int
	contestants contestantList
	checkpoints []int
	finish      chan interface{}
	sync.RWMutex
}

// NewRace creates instance of a new fictional race with count
// of contestants (skiers) running against defined checkpoints
func NewRace(count int, checkpoints []int) *Race {
	r := &Race{
		contestants: make([]contestant, count),
		checkpoints: checkpoints,
	}
	return r
}

// ServeHTTP produces XML report of the race. XML feed has 10 percent
// probability of failed requests to emulate network problems
func (r *Race) ServeHTTP(w http.ResponseWriter, rq *http.Request) {

	if rand.Int31n(10) == 0 {
		// 10% of calls failed to test receiver
		time.Sleep(30 * time.Second)
		w.WriteHeader(http.StatusGatewayTimeout)
		bytes.NewBufferString("Fake time out").WriteTo(w)
		return
	}

	if rand.Int31n(3) == 0 {
		// 33% calls in average will be delayed over client timeout (5sec)
		time.Sleep(time.Duration(rand.Int63n(15000)) * time.Millisecond)
	}

	r.RLock()
	points := len(r.checkpoints)
	res := snapshotXML{}
	res.Checkpoints = make([]checkpoint, points)
	for i := 0; i < points; i++ {
		cp := &res.Checkpoints[i]
		cp.Distance = r.checkpoints[i]
		cp.Name = fmt.Sprintf("Kontrolní bod %0.2g km", float64(r.checkpoints[i])/1000.0)
		for _, c := range r.contestants {
			if int(c.Distance) > cp.Distance {
				cp.Count++
			}
		}
	}
	defer r.RUnlock()

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-type", "text/xml")
	e := xml.NewEncoder(w)
	e.EncodeElement(res, xml.StartElement{Name: xml.Name{Local: "splits"}})
}

// AlreadyStarted returns true if the race is running
func (r *Race) AlreadyStarted() bool {
	r.RLock()
	defer r.RUnlock()
	return r.finish != nil
}

// Start runs background goroutine with simulated contestants
func (r *Race) Start() {
	r.Lock()
	defer r.Unlock()

	if r.finish != nil {
		return
	}

	r.finish = make(chan interface{})
	for i := range r.contestants {
		r.contestants[i].Distance = 0
	}

	raceLength := float64(r.checkpoints[len(r.checkpoints)-1])
	log.Printf("Race length: %f", raceLength)

	// approx 2% is elite
	for i := 0; i < len(r.contestants)/50; i++ {
		r.contestants[i].Speed = raceLength / ((130.0 + float64(rand.Intn(50))) * 60.0)
		r.onTrack++
	}

	go func() {
		// elite group is 20 seconds ahead before the first wave
		time.Sleep(20 * time.Second)
		for {
			log.Printf("MOCK: new wave of contestants is about to start...")
			for j := 0; j < 13; j++ {
				r.Lock()
				for i := 0; i < 50; i++ {
					r.contestants[r.onTrack].Speed = raceLength / ((160 + float64(rand.Intn(180))) * 60)
					r.onTrack++
					if r.onTrack == len(r.contestants) {
						log.Printf("MOCK: all contestants are on track")
						r.Unlock()
						return
					}
				}
				r.Unlock()
				// 50 skiers every 5 seconds
				sleep := time.After(time.Second * 10)
				select {
				case <-r.finish:
					return
				case <-sleep:
				}
			}
			// every 5 minutes a new wave is onTrack (300-130)
			sleep := time.After(time.Second * 170)
			select {
			case <-r.finish:
				return
			case <-sleep:
			}
		}
	}()

	lastContestantsInRace := 0
	go func() {
		for {
			sleep := time.After(time.Second * 1)
			contestantInRace := 0
			select {
			case <-sleep:
				r.Lock()
				for i := 0; i < r.onTrack; i++ {
					if r.contestants[i].Distance < raceLength {
						r.contestants[i].Distance += r.contestants[i].Speed
						contestantInRace++
					}
				}
				r.Unlock()
				if contestantInRace == 0 {
					// all contestants finished race
					return
				}
			case <-r.finish:
				// race was interrupted
				return
			}
			if lastContestantsInRace != contestantInRace {
				log.Printf("MOCK running contestants %d", contestantInRace)
				lastContestantsInRace = contestantInRace
			}
		}
	}()

	return
}

// Finish kills current race
func (r *Race) Finish() {
	r.Lock()
	defer r.Unlock()
	close(r.finish)
	r.finish = nil
}
