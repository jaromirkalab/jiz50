# Jizerská 50 #

## Routes ##

* `/tvfeed` - TV graphics with animation
* `/tv` - just counter
* `/news` - a little bit shoddy page so far
* `/admin/` - backend info page


## Env ##

* `HOSTPORT` - <host>:<port> string for server [default is `localhost:8585`]
* `SELF_RACE` - start virtual resource for testing on URL `http://<host>:<port>/source`
* `FEED_URL` - absolute URL for XML feed (except `SELF_RACE` is activated)
* `HTML_DIR` - HTML dir [default `./html`]
* `ASSETS_DIR` - assets dir (with js,img,css,fonts subdirectories) [default is `./assets`]
* `TEMPLATES_DIR` - go templates for admin page [default is `./templates`]

## Feed ##

Feed is peridically grabbed from `FEED_URL` (or self simulator) in the following XML form:

```
<splits>
   <split distance="0" count="4030" name="Start"/>
   <split distance="11200" count="4030" name="11 km"/>
   <split distance="22800" count="4017" name="26 km"/>
   <split distance="37100" count="4018" name="37 km"/>
   <split distance="45000" count="4026" name="Finish"/>
</splits>
```

The `count` attribude is number of skiers that reached checkpoint at `distance`. The `name`
attribute is only informational. Then number of checkpoint is variable but it must be constant
during the race.

The skiers at start point are counted with 10 metres (just to add some value to total sum).

Because there is a risk that some skiers can cross finish line with activated chips before they actually
started, the finish counter is blosked unless at leas one skiers cross
the last checkpoint ahead of finish checkpoint.

## Other ##

* Result of Jizerska 50 2020 available [here](./doc/2020.md)
