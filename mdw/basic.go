package mdw

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

var basicAuthenticationHeader = regexp.MustCompile("^Basic (\\S+)$")

type BasicAuth struct {
	realm     string
	authorize func(login, pwd string) bool
}

func NewBasicAuthenticator(realm string, authFunc func(string, string) bool) *BasicAuth {
	return &BasicAuth{
		realm:     realm,
		authorize: authFunc,
	}
}

func (ba *BasicAuth) authRequired(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", fmt.Sprintf("Basic realm=\"%s\"", ba.realm))
	http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
	return
}

func (ba *BasicAuth) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := basicAuthenticationHeader.FindStringSubmatch(r.Header.Get("Authorization"))

		if len(auth) != 2 {
			ba.authRequired(w)
			return
		}
		data, err := base64.StdEncoding.DecodeString(auth[1])
		if err != nil {
			ba.authRequired(w)
			return
		}
		parts := strings.Split(string(data), ":")

		if len(parts) == 2 && ba.authorize(parts[0], parts[1]) {
			next.ServeHTTP(w, r)
			return
		}
		ba.authRequired(w)
	})
}
