odometerOptions = { 
	auto: false,
};

var Counters = {
	
	total: null,
		
	init: function() {

		this.total = $('.count-total');
        this.initOdometer(this.total);
	},

	/**
	 * nastavi js pocitadla na body
	 */
	initOdometer: function(element) {
		var value = element.text();

		if (!element || !element.length)
			return false;

		var odometer = new Odometer({
			el: element[0],
			format: ' ddd',
			duration: 1200,
			value: value
		});
		odometer.render();
	},

	
	/**
	 * nastavi celkove body
	 */
	setTotal: function(points) {
		this.total.html(points);
	},
	
};

var WebSocketClient = {
	

        url: (window.location.protocol === 'https:' ? 'wss': 'ws' ) + '://' + window.location.host + '/ws',
	websocket: null,
	
	init: function() {
		this.websocket = new WebSocket(this.url);
		this.websocket.onopen = this.onOpen;
		this.websocket.onclose = this.onClose;
		this.websocket.onmessage = this.onMessage;
		this.websocket.onerror = this.onError;
	},
	
	onOpen: function(e) {
	},
	
	onClose: function(e) {
		setTimeout(function() { WebSocketClient.init(); }, 500);
	},
	
	onMessage: function(e) {
		var data = JSON.parse(e.data);
		Counters.setTotal(data.distance);
	},
	
	onError: function(e) {
//		setTimeout(function() { WebSocketClient.init(); }, 500);
	},
	
	
};

$(function() {
	
	Counters.init();
	WebSocketClient.init();
	
});
